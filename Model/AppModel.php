<?php

class AppModel {
    protected $table;
    protected $dbh;

    public function __construct() {
        $this->table = strtolower(get_class($this));
        $this->dbh = $GLOBALS['database']['dbh'];
    }

    public function save($data, $fields = null) {
        $data = "'".implode("', '", $data)."'";

        if($fields != null) {
            $fields = implode(", ", $fields);
            $req = $this->dbh->prepare("INSERT INTO " . $this->table . "(" . $fields . ") VALUES(" . $data . ")");
        } else {
            $req = $this->dbh->prepare("INSERT INTO " . $this->table . " VALUES('', " . $data . ")");
        }

        if($req->execute()) return true;
        else return false;
    }

    public function findAll() {
        $queryAnswer = $this->dbh->query("SELECT * FROM ".$this->table);
        $data = $queryAnswer->fetchAll();
        return $data;
    }

    public function findById($id) {
        $queryAnswer = $this->dbh->query("SELECT * FROM ".$this->table." WHERE id=".$id);
        $data = $queryAnswer->fetch();
        return $data;
    }

    public function updateById($id, $d) {
        /* Not yet implemented */
    }

    public function delete($id) {
        $req = $this->dbh->prepare("DELETE FROM ".$this->table." WHERE id = ".$id);
        $req->execute();
    }

    public function getLast($nb) {
        $queryAnswer = $this->dbh->query("SELECT * FROM ".$this->table." ORDER BY id DESC LIMIT ".$nb);
        $data = $queryAnswer->fetchAll();
        return $data;
    }
}