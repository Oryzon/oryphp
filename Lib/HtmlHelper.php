<?php

class HtmlHelper {
    private $cssFolder;
    private $jsFolder;

    protected $CSS = array();
    protected $JS = array();

    function __construct() {
        $this->cssFolder = 'Webroot/css/';
        $this->jsFolder = 'Webroot/js/';
    }

    function css($styleToCall = null) {
        if($styleToCall == null) {
            $toReturn = "";
            foreach($this->CSS as $style) { $toReturn .= '<link rel = "stylesheet" href="'. $style .'" />'; }
            echo $toReturn;
        } else if(file_exists($this->cssFolder . $styleToCall . '.css')) {
            $this->CSS[count($this->CSS)] = $this->cssFolder . $styleToCall  . '.css';
        } else echo "Error : HtmlHelper -> CSS not found.";
    }

    function getCss() {
        return $this->CSS;
    }

    function js($scriptToCall) {
        if($scriptToCall == null) {
            $toReturn = "";
            foreach($this->JS as $script) { $toReturn .= '<script src="'.$script.'"></script>'; }
            echo $toReturn;
        } else if(file_exists($this->jsFolder . $scriptToCall . '.js')) {
            $this->JS[count($this->JS)] = $this->jsFolder . $scriptToCall  . '.js';
        } else echo "Error : HtmlHelper -> JS not found.";
    }

    function getJs() {
        return $this->JS;
    }
}