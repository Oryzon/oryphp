<?php

if($database['Activate']) {
    $dsn = 'mysql:host=' . $database['Host'] . ';dbname=' . $database['Name'];

    try {
        $database['dbh'] = new PDO($dsn, $database['User'], $database['Pswd'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

    } catch (PDOException $e) {
        echo "erreur connexion";
        die();
    }
}