<?php

/**
 *
 *  ___   ____   __ __  ____  __ __  ____
 * /   \ |    \ |  |  ||    \|  |  ||    \
 * |     ||  D  )|  |  ||  o  )  |  ||  o  )
 * |  O  ||    / |  ~  ||   _/|  _  ||   _/
 * |     ||    \ |___, ||  |  |  |  ||  |
 * |     ||  .  \|     ||  |  |  |  ||  |
 *  \___/ |__|\_||____/ |__|  |__|__||__|
 *
 * Hi ! Welcome on OryPHP !
 * This is the only file where you can edit something in Core directory.
 *
 * It's the configuration of you website, for database, if it's the first time or not, also for emailing !
 *
 * In this, you can enable module of the framework (the list of module create is below), and configure module !
 *              [Module]    - Database
 *                          - Email         - Not yet implemented
 *                          - HtmlHelper    - Not yet implemented
 *                          - Dev mode      - Not yet implemented
 *                          - Admin panel   - Not yet implemented
 *                          - Soon, more.
 *
 * Have fun with this framework.
 *                                                                                              ~ Oryzon
 */

/**
 * Right now, this framework is in "First Time Using", so, for create your website, please, change this to 'false' :
 */
$FTU = true;

/**
 * What is the default controller of the site?
 *
 * This is usefull for your URL, by example, create a controller Home, and to access at this, you type localhost/ and you use the HomeController
 */
$defaultController = "Home";

/**
 * Module :
 *          Database
 *
 * The module is activate? (default, true)
 */
$database = array(
    'Activate' => true,
    'Host' => 'localhost',
    'Name' => 'oryphp',
    'User' => 'root',
    'Pswd' => 'root'
);


/**
 * Module : Not yet implemented
 *          Email
 *
 * The module is activate? (default, false)
 */
$emailActivate = false;

/**
 * Module : Not yet implemented
 *          Dev mode
 *
 * The module is activate? (default, true)
 */
$devmodeActivate = true;

/**
 * Module : Not yet implemented
 *          Admin panel
 *
 * Description :
 *          This module pre-create the controller for Admin Panel. So you have a panel already create (just design), and now, it's at you to create function and everything else.
 *
 * The module is activate? (default, false)
 */
$adminpanelActivate = false;
