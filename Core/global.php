<?php

include('routes.php');

// Configuration files
require(ROOT.'Core/config.php');

// HtmlHelper
require(ROOT . 'Lib/HtmlHelper.php');

// Now what's to do with module which are activate
if($database['Activate']) require(ROOT.'Core/database.php');

// Required files, default files for controller and models
require(ROOT.'Model/AppModel.php');
require(ROOT.'Controller/AppController.php');


if($FTU) {
    $controller = new AppController();
    $controller->index();
} else {
    $params = explode('/', $_GET['p']);
    $tmpParams = array();

    if(count($params) == 1 && !isset($params[1]) || empty($params[1])) {
        $controller = $defaultController;
        $action = (isset($params[0]) && !empty($params[0])) ? $params[0] : 'index';
    } else {
        $controller = ucfirst($params[0]);
        $action = (isset($params[1]) && !empty($params[1])) ? $params[1] : 'index';
    }

    if (file_exists('Controller/' . $controller . 'Controller.php')) {
        require('Controller/' . $controller . 'Controller.php');
        $controller = new $controller();
    } else {
        echo 'erreur controller';
        return false;
    }

    if (method_exists($controller, $action)) {
        $controller->$action();
    }
    else {
        echo 'erreur action';
        return false;
    }
}