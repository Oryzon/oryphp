<?php

class AppController {
    private $vars = array();
    private $layout = 'default';

    protected $HtmlHelper;

    public function __construct() {
        if($this == "AppController") $this->loadModel('AppModel');
        else $this->loadModel($this . 's');
        
        $this->HtmlHelper = new HtmlHelper();
    }

    /**
     * Method to : Have the name of the controller, in a String.
     * @return string
     */
    public function __toString()
    {
        return get_class($this);
    }

    /**
     * Method to : Set vars in view.
     * @param $d
     */
    public function set($d) {
        $this->vars = array_merge($this->vars, $d);
    }

    /**
     * Method to : Render the view.
     * @param $filename
     */
    public function render($filename) {
        // Extract vars initiate by developper.
        extract($this->vars);

        ob_start();
        require(ROOT.'View/'.get_class($this).'/'.$filename.'.php');
        $content_for_layout = ob_get_clean();

        if($this->layout == false) echo $content_for_layout;
        else require(ROOT.'View/Layout/'.$this->layout.'.php');
    }

    /**
     * Method to : Change the layout.
     * @param $filenameLayout
     */
    public function layout($filenameLayout) {
        $this->layout = $filenameLayout;
    }

    /**
     * Method to : Debugging.
     * @param $toDebug
     */
    public function debug($toDebug) {
        /* Not yet implemented ! */
    }

    /**
     * Method to : Load a specific model, or the model of the controller.
     * @param null $name
     */
    public function loadModel($name = null) {
        require_once(ROOT.'Model/'.$name.'.php');
        $this->$name = new $name();
    }

    public function index() {
        $this->HtmlHelper->css('default');
        ob_start();
        require(ROOT.'View/Default/welcome.php');
        $content_for_layout = ob_get_clean();

        if($this->layout == false) echo $content_for_layout;
        else require(ROOT.'View/Layout/default.php');
    }
}