<h1>Welcome !</h1>

<p>If you see this page, it's because it's your first using of the framework.</p>

<p>For start using the framework, you must update some things.</p>

<p>Here is the list :</p>

<ul>
    <li>Change the value of $FTU in /Core/config.php - line 33.</li>
    <li>Create a controller in /Controller called (by example) HomeController.php which extends AppController.</li>
    <li>Create a model in /Model called (by example) Homes.php (the 's' is important !) which extends AppModel.</li>
    <li>Create a folder named (by example) Home if the folder View.</li>
    <li>In /Core/config.php - line 40, change the value of $defaultController, for pointing on your first controller.</li>
    <li>In /Core/config.php - line 48, change the value of $database, for using your database.</li>
    <li>And I think it's all.</li>
</ul>

<p>Now, look the code, if you have some trouble, you can contact me at oryphp@oryzon.me, by the framework is very simple (and not finished!)</p>

<h1>Enjoy ! :)</h1>