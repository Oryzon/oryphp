<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>OryPHP</title>

        <?php $this->HtmlHelper->css(); ?>

    </head>

    <body>
        <div id="header">
            <p id="headTitle">OryPHP</p>
        </div>

        <div id="content">
            <?php echo $content_for_layout; ?>
        </div>

        <div id="footer">
            <p id="footerText">Created by <a href="https://twitter.com/Oryzon_/">@Oryzon_</a></p>
        </div>
    </body>
</html>